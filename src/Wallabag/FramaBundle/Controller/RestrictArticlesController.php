<?php

namespace Wallabag\FramaBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RestrictArticlesController extends Controller {

    /**
     * @Route("restrict/articles", name="restrict_articles_index")
     */
    public function restrictArticlesIndexAction()
    {
        $nbEntries = $this->getDoctrine()->getManager()->getRepository('WallabagCoreBundle:Entry')
            ->countAllEntriesByUser($this->getUser()->getId());

        $nbArchivedEntries = $this->getDoctrine()->getManager()->getRepository('WallabagCoreBundle:Entry')
            ->countArchivedEntriesByUser($this->getUser()->getId());

        return $this->render('WallabagFramaBundle:Index:index.html.twig', [
            'nb_articles' => $nbEntries,
            'nb_archived_articles' => $nbArchivedEntries,
        ]);
    }
}